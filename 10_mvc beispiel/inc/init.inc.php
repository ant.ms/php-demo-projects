<?php
/*
 * Autor: Tobias Peter
 * Datum: 20.03.2018
 * Projekt: MVC vorstellen
 * Dateiname: init.inc.php
 * Zweck: Initialisiert die Umgebung der Applikation
 */

// Konstanten aufrufen um Grundelemente laden zu können
if(file_exists(__DIR__."/constants.inc.php")) {
	require_once(__DIR__."/constants.inc.php");
} else {
	die("Datei mit Konstanten nicht gefunden!");
}

// Funktionen aufrufen um Grundelemente laden zu können
if(file_exists(__DIR__."/".FUNCTIONS_FILE)) {
	require_once(__DIR__."/".FUNCTIONS_FILE);
} else {
	die("Datei mit Funktionen nicht gefunden: ".__DIR__."/".FUNCTIONS_FILE);
}

// HTML Header laden
$header_view = new View("header");

?>