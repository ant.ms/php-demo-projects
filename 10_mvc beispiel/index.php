<?php
/*
 * Autor: Tobias Peter
 * Datum: 20.03.2018
 * Projekt: MVC vorstellen
 * Dateiname: index.php
 * Zweck: Erste aufgerufene Datei
 */

// Session initialisieren
session_start();

// init.inc.php aufrufen um gesamte Umgebung zu laden
require_once(__DIR__."/inc/init.inc.php"); // __FILE__

// Für Startseite entsprechende View laden
$index_view = new View("index");

// HTML Footer zum Ende laden
$footer_view = new View("footer");
?>