<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>3-12</title>
    </head>
    <body>
        <form action="3-12.php" method="post">
            <input type="number" name="decimal" id="decimal" placeholder="Enter Number in Decimal">
            <button type="submit">Convert</button>
        </form><br>
    </body>
</html>
<?php
    if (isset($_POST['decimal'])) {
        conversion();
    } 
    function conversion() {
        $decimal = $_POST["decimal"];
        echo "Umrechnung der Dezimalzahl: $decimal" . "<br>";
        echo "ins Binärsystem: " . decbin($decimal) . "<br>";
        echo "ins Hexadezimalsystem: " . dechex($decimal) . "<br><br>";
    }
?>
