<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>16-6</title>
    </head>

    <body>
        <nav>
            <div class="nav-wrapper">
                <a href="." class="brand-logo center">16-6 - Database Interface</a>
            </div>
        </nav>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                // Create connection
                $connection = mysqli_connect("127.0.0.1","phpmodul","Siemens_01","phpmodul");
                // Check connection
                if (!$connection) {
                    die("Connection failed: " . mysqli_connect_error());
                }

                $sql = "INSERT INTO messageboard (name, email, message, datum) VALUES ('" . 
                $_POST['name'] . "', '" . $_POST['email'] . "', '" . $_POST['message'] . "', '" . date('Y-m-d') . "')";

                if (mysqli_query($connection, $sql)) {
                    echo "<script>M.toast({html: 'Added Entry successfully!'})</script>";
                } else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($connection);
                }

                mysqli_close($connection);
            } else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                if (isset($_GET['delete'])) {
                    // Create connection
                    $connection = mysqli_connect("127.0.0.1","phpmodul","Siemens_01","phpmodul");
                    // Check connection
                    if (!$connection) {
                        die("Connection failed: " . mysqli_connect_error());
                    }

                    if (mysqli_query($connection, "DELETE FROM messageboard WHERE ID=" . $_GET['delete'] . ";")) {
                        echo "<script>M.toast({html: 'Deleted entry successfully!'})</script>";
                    } else {
                        echo "<script>M.toast({html: 'Failed to delete Entry! Already deleted?'})</script>";
                    }

                    mysqli_close($connection);
                }
            }
        ?>


        <div style="height: 10px"></div>


        <div class="container">
            <div class="row">
                <form class="col s12" action="16-6.php" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                        <input id="name" type="text" class="validate" name="name">
                        <label for="name">Name</label>
                        </div>
                        <div class="input-field col s6">
                        <input id="email" type="email" class="validate" name="email">
                        <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <input id="message" type="text" class="validate" name="message">
                        <label for="message">Message</label>
                        </div>
                    </div>
                    <div class="row">
                    <a class="waves-effect waves-light btn" onclick="document.getElementsByTagName('form')[0].submit()">Add to Database <i class="material-icons right">send</i></a>
                    </div>
                </form>
            </div>
            <div class="row">
                <table class="highlight">
                    <?php
                        $mysqli = new mysqli("127.0.0.1","phpmodul","Siemens_01","phpmodul");

                        // Check connection
                        if ($mysqli -> connect_errno) {
                            echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
                            exit();
                        }

                        if ($result = $mysqli -> query("SELECT * FROM messageboard")) {
                            /* fetch associative array */
                            while ($row = $result -> fetch_assoc()) {
                                echo "<tr><td>" . $row['ID'] . "</td><td>" . $row['name'] . "</td><td><a href=\"mailto:". $row['email'] . "\">" . $row['email'] . "</a></td><td> ". $row['message'] . "</td><td>" . $row['datum'] . "</td><td><i onclick=\"window.location.href = '16-6.php?delete=" . $row['ID'] ."';\" class=\"material-icons right\">delete</i></td></tr>";
                            }
                            $result->free();
                        }
                        $mysqli -> close();
                    ?>
                </table>
            </div>
        </div>
    </body>
</html>