<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>3-13</title>
    </head>
    <body>
        <form action="3-13.php" method="post">
            <input type="radio" name="choice" value="1" checked="checked">Würfeln (1-6)
            <input type="radio" name="choice" value="2">Lottozahlen (1-49)
            <button type="submit">Würfeln</button>
        </form><br>
    </body>
</html>
<?php
    if (isset($_POST['choice'])) {
        switch ($_POST['choice']) {
            case 1:
                throwDice();
                break;
            
            case 2:
                getLottoNumbers();
                break;
        }
    }
    function throwDice() {
        echo rand(1,6);
    }
    function getLottoNumbers() {
        $numbers = array(rand(1, 49));
        echo $numbers[0] . "<br>";
        for ($i=1; $i < 6; $i++) { 
            $random = rand(1, 49);
            while (isExistant($numbers, $random) == true) {
                $random = rand(1, 49);
            }
            $numbers[] = $random;
            echo $random . "<br>";
        }
    }
    function isExistant($numbers, $random) {
        for ($j=0; $j < sizeof($numbers); $j++) { 
            if ($random == $numbers[$j]) {
                return true;
            }
        }
        return false;
    }
?>