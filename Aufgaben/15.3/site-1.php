<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
        <nav>
            <div class="nav-wrapper">
                <a href="#" class="brand-logo center">Site 1</a>
            </div>
        </nav>

        <div style="height: 10px"></div>

        <div class="container">
            <a class="waves-effect waves-light btn" href="site-2.php">Site 2</a>
            <a class="waves-effect waves-light btn" href="site-3.php">Site 3</a>
        </div>

        <!--JavaScript at end of body for optimized loading-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
    <?php
        require_once 'log.inc.php';
        log_visit("site-1.php");
    ?>
</html>