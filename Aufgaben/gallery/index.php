<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Gallery</title>
    </head>
    
    <body>
        <nav class="blue">
            <div class="nav-wrapper">
                <a href="." class="brand-logo center">Gallery</a>
            </div>
        </nav>

        <div style="height: 10px"></div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <?php
        if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
            if ($_FILES["file"]["error"] > 0) {
                echo "<script>M.toast({html: 'Failed to upload image!!!'})</script>";
            } else {
                move_uploaded_file($_FILES["file"]["tmp_name"], "src/" . $_FILES["file"]["name"]);
                echo "<script>M.toast({html: 'Successfully uploaded image'})</script>";
            }
        ?>

        <div class="container">
            <div class="row">
                <?php

                $files = array_filter(glob("src/*"), 'is_file');
                foreach($files as $file) {
                    echo   "<div class=\"col s12 m4\">\n";
                    echo       "<div class=\"card\">\n";
                    echo           "<div class=\"card-image\">\n";
                    echo               "<img class=\"materialboxed\" src=\"$file\">\n";
                    echo           "</div>\n";
                    // echo           "<div class=\"card-action\">\n";
                    // echo                "<a href=\"#\">Delete this image <i class=\"material-icons right\">delete</i></a>\n";
                    // echo           "</div>\n";
                    echo       "</div>\n";
                    echo    "</div>\n";;
                }

                ?>
            </div>
        </div>

        <div class="fixed-action-btn">
            <form action="index.php" method="post" id="myForm" enctype="multipart/form-data">
                <input type="file" name="file" id="file" accept="image/gif, image/jpeg, image/png" style="position: absolute;left: 5000px" oninput="document.getElementById('myForm').submit();">
            </form>
            <button class="btn waves-effect waves-light" id="uploadButton" onclick="document.getElementById('file').click();">Upload
                <i class="material-icons right">backup</i>
            </button>
        </div>

        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var elems = document.querySelectorAll('.materialboxed');
                var instances = M.Materialbox.init(elems);
            });
        </script>
    </body>
</html>
