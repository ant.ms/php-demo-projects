<?php
    function rows($count) {
        for ($i=0; $i < $count; $i++) { 
            echo "Zeile<br>";
        }
    }
    echo rows(4);
    echo "<br>";

    function rows1($count) {
        for ($i=0; $i < $count; $i++) { 
            echo "Zeile $i<br>";
        }
    }
    echo rows1(4);
    echo "<br>";

    function rows2($count) {
        $array = array("Peter", "Paul", "Pascal", "Fred");
        for ($i=0; $i < $count; $i++) { 
            echo "$array[$i]<br>";
        }
    }
    echo rows2(4);
?>