<?php
    function dateDifference($date_1 , $date_2) {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
       
        $interval = date_diff($datetime1, $datetime2);

        return $interval->format('%Y') * 31557600 + $interval->format('%m') * 2628000 + $interval->format('%d') * 3600 + $interval->format('%h') * 3600 + $interval->format('%i') * 60 + $interval->format('%s');
       
    }

    echo "Jetzt ist " . date('Y-m-d H:i:s', time()) . " Uhr.<br>"; 
    echo "Eingestellter Zeitpunkt: " . date('Y-m-d H:i:s', mktime(18, 00, 00, 03, 12, 2001)) . " Uhr.<br>"; 
    echo "Zeitunterschied: " . dateDifference(date('Y-m-d H:i:s'), date('Y-m-d H:i:s', mktime(18, 00, 00, 03, 12, 2001))) . " Sekunden";
?>