<?php

if( isset($_GET['dologin']) ) {
    if ($_GET['dologin'] == true) {
        if (hash("sha512", $_POST["email"] . $_POST["password"]) == "f69ee2863f87ab8e9db6f8551ede1a48189c29eb51eec6ee8346ea06da255fac3198ce74cef798a8c6d773dc059e8bf74f6ad99691f13bb9efb425e38be81dbf") {
            setcookie('email', $_POST["email"], time() + (86400 * 30), "/");
            setcookie('password', $_POST["password"], time() + (86400 * 30), "/");
            echo "<meta http-equiv=\"refresh\" content=\"0;url=.\">";
        } else {
            echo "<meta http-equiv=\"refresh\" content=\"0;url=.?wrongpassword=true\">";
        }
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <title>Admin for Blog</title>
    </head>

    <body>
        <nav style="padding-left: 15px" class="teal">
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href=".." class="breadcrumb">Home</a>
                    <a href="." class="breadcrumb">Admin</a>
                </div>
            </div>
        </nav>


        <div style="height: 10px"></div>


        <div class="container">
            <?php
                if (isset($_GET['wrongpassword'])) {
                    if ($_GET['wrongpassword'] == true)
                        echo "<h1>Wrong password or email, you will now be redirected to the loginscreeen...</h1>";
                        echo "<meta http-equiv=\"refresh\" content=\"2;url=.\">";
                } else {
                    if(!isset($_COOKIE['email'])) {
                        // show login page
                        echo "<h1>Login</h1>";
                        echo "
                        <div class='row'>
                        <form action='index.php?dologin=true' method='post'>
                            <div class='input-field col s12'>
                                <input name='email' id='email' type='email' class='validate'>
                                <label class='active' for='email'>Email</label>
                            </div>
                            <div class='input-field col s12'>
                                <input name='password' id='password' type='password' class='validate'>
                                <label class='active' for='password'>Password</label>
                            </div>
                            <button class='btn waves-effect waves-light' type='submit' name='action'>Submit
                                <i class='material-icons right'>send</i>
                            </button>
                        </form>
                        </div>";
                    } else {
                        // check if correct password
                        if (hash("sha512", $_COOKIE["email"] . $_COOKIE["password"]) != "f69ee2863f87ab8e9db6f8551ede1a48189c29eb51eec6ee8346ea06da255fac3198ce74cef798a8c6d773dc059e8bf74f6ad99691f13bb9efb425e38be81dbf") {
                            echo "<meta http-equiv=\"refresh\" content=\"0;url=.?wrongpassword=true\">";
                        } else {
                            // login correct
                            echo "<br><br><br><div class='row'>";
                            echo    "<form class='col s12' action='write.php' method='post'>";
                            echo        "<div class='row'>";
                            echo            "<div class='input-field col s8'>";
                            echo                "<input name='title' id='title' type='text' data-length='10'>";
                            echo                "<label for='title'>Title</label>";
                            echo            "</div>";
                            echo            "<div class='input-field col s4'>";
                            echo                "<select name='type' id='type' required>";
                            echo                    "<option value='posts' selected>Post</option>";
                            echo                    "<option value='pages'>Page</option>";
                            echo                "</select>";
                            echo                "<label>Materialize Select</label>";
                            echo            "</div>";
                            echo            "</div>";
                            echo            "<div class='row'>";
                            echo                "<div class='input-field col s12'>";
                            echo                "<textarea name='content' id='content' class='materialize-textarea'></textarea>";
                            echo                "<label for='content'>Content</label>";
                            echo            "</div>";
                            echo            "<button class='btn waves-effect waves-light' type='submit' name='action'>Submit";
                            echo                "<i class='material-icons right'>send</i>";
                            echo            "</button>";
                            echo        "</div>";
                            echo    "</form>";
                            echo "</div>";
                        }
                    }
                }
            ?>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var elems = document.querySelectorAll('select');
                var instances = M.FormSelect.init(elems);
            });
        </script>
    </body>
</html>