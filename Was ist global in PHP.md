globale variabeln müssen mit `global <variabelnname>;` in einer Funktion initialisiert werden damit sie lokal genutzt werden können.

```php
$greeting = "Hey"; // globales scope

function getGreeting() {
    global $greetings; // referenziert lokales scope zu globalem scope
    echo $greeting;
}
```
